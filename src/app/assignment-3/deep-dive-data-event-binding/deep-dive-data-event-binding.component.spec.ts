import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeepDiveDataEventBindingComponent } from './deep-dive-data-event-binding.component';

describe('DeepDiveDataEventBindingComponent', () => {
  let component: DeepDiveDataEventBindingComponent;
  let fixture: ComponentFixture<DeepDiveDataEventBindingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeepDiveDataEventBindingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeepDiveDataEventBindingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
