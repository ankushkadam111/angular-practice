import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-deep-dive-data-event-binding',
  templateUrl: './deep-dive-data-event-binding.component.html',
  styleUrls: ['./deep-dive-data-event-binding.component.scss']
})
export class DeepDiveDataEventBindingComponent implements OnInit {

  taskList: string[] = [
    'Create 3 component:  gameControl, Odd, Even',
    'GameControl component have button to start and stop game',
    'When starting the game, an event (holding increment number) should get emitted each second (ref = setInterval())',
    'event should be listanble from outside the component',
    'when stoping the game no more event should get emmited (clearInterval)',
    'a new odd component should get created for every odd number emmited, same should happen for even component',
    'simply output odd and even in two component',
    'style the paragraph holding your output text differently in both component'
  ];

  oddNumbers: number[] = [];
  evenNumbers: number[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  emittedEvent(no: number): void{
    if (no % 2 === 0){
      this.evenNumbers.push(no);
    } else {
      this.oddNumbers.push(no);
    }
    console.log(`event no is ${this.evenNumbers} and oddNumbers is ${this.oddNumbers}`);
  }

  getValue(): string {
    
    return 'abc';
  }
}
