import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.scss']
})
export class GameControlComponent implements OnInit {

  @Output() emitNumberEvent = new EventEmitter<number>();

  intervalRef: any;
  constructor() { }

  ngOnInit(): void {
  }

  startGame(): void {
    let  initialNumber = 0;
    this.intervalRef = setInterval(() => {
      this.emitNumberEvent.emit(initialNumber + 1);
      initialNumber ++;
    }, 1000);
  }

  stopGame(): void {
    clearInterval(this.intervalRef);
  }
}
