import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  styleUrls: ['./databinding.component.scss']
})
export class DatabindingComponent implements OnInit {

  userName: string  = '';

  constructor() { }

  ngOnInit(): void {
  }

  reset(): void {
    this.userName = '';
  }
}
