import { Component, OnInit } from '@angular/core';
import { User, UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-inactive-user',
  templateUrl: './inactive-user.component.html',
  styleUrls: ['./inactive-user.component.scss']
})
export class InactiveUserComponent implements OnInit {

  users: User[] | undefined;
  constructor(
    private userService: UserServiceService
  ) { }

  ngOnInit(): void {
    this.users = this.userService.users;
  }

  setToActive(user: User): void {
    this.userService.setToActive(user);
  }
}
