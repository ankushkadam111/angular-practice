import { Injectable } from '@angular/core';

export interface User {
  name: string;
  active: boolean;
}

@Injectable()
export class UserServiceService {
  users: User[] = [
    {name: 'Rama', active: true},
    {name: 'Nilesh', active: false},
    {name: 'Aksahy ', active: true},
    {name: 'Ankush', active: false},
    {name: 'Shree', active: true},
  ];

  constructor() { }

  setToInactive(user: User): void {
    user.active = false;
  }
  setToActive(user: User): void {
    user.active = true;
  }
}
