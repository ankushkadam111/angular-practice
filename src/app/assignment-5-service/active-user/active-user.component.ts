import { Component, OnInit } from '@angular/core';
import { User, UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-active-user',
  templateUrl: './active-user.component.html',
  styleUrls: ['./active-user.component.scss']
})
export class ActiveUserComponent implements OnInit {

  users: User[] | undefined;
  dummyObj = {
    name: 'ankush',
    mobile: 9985645216,
    birth: '29 Aug',
    school: 'hire high school',
    college: 'jedhe college',
    degree: 'bcs',
    'passout year': 2016,
    hometown : 'Pune'
  };
  
  constructor(
    private userService: UserServiceService
  ) { }

  ngOnInit(): void {
    this.users = this.userService.users;
  }

  setToInactive(user: User): void {
    this.userService.setToInactive(user);
  }
}
