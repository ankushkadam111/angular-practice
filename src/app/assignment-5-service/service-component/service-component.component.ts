import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';

@Component({
  selector: 'app-service-component',
  templateUrl: './service-component.component.html',
  styleUrls: ['./service-component.component.scss'],
  providers: [UserServiceService]
})
export class ServiceComponentComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
