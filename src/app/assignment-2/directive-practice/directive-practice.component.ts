import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directive-practice',
  templateUrl: './directive-practice.component.html',
  styleUrls: ['./directive-practice.component.scss']
})
export class DirectivePracticeComponent implements OnInit {

  paragraph = 'Just a Paragraph.';
  isDisplay = true;
  logs: any[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onClick(): void{
    this.isDisplay = !this.isDisplay;
    this.logs.push(this.logs.length + 1);
  }

  getColor(i: number): string{
    return i + 1 >= 5 ? 'blue' : 'transparent';
   }
}
