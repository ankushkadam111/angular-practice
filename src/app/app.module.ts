import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DatabindingComponent } from './assignment-1/databinding/databinding.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DirectivePracticeComponent } from './assignment-2/directive-practice/directive-practice.component';
import { DeepDiveDataEventBindingComponent } from './assignment-3/deep-dive-data-event-binding/deep-dive-data-event-binding.component';
import { GameControlComponent } from './assignment-3/game-control/game-control.component';
import { OddComponent } from './assignment-3/odd/odd.component';
import { EvenComponent } from './assignment-3/even/even.component';
import { ServiceComponentComponent } from './assignment-5-service/service-component/service-component.component';
import { ActiveUserComponent } from './assignment-5-service/active-user/active-user.component';
import { InactiveUserComponent } from './assignment-5-service/inactive-user/inactive-user.component';

@NgModule({
  declarations: [
    AppComponent,
    DatabindingComponent,
    DirectivePracticeComponent,
    DeepDiveDataEventBindingComponent,
    GameControlComponent,
    OddComponent,
    EvenComponent,
    ServiceComponentComponent,
    ActiveUserComponent,
    InactiveUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
